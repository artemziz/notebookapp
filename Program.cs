﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class NoteBook
    {
        static void EndPart()
        {
            Console.WriteLine("Нажмите любую клавишу чтобы продолжить...");
            Console.ReadLine();
            Console.Clear();
        }
        static bool IfExist(List<Note> notes, int id,out Note note)
        {
            
            note = null;
  
            foreach (Note value in notes)
            {
                if (value.id == id)
                {
                    note = value;
                }
            }

            if (note == null)
            {
                Console.WriteLine("Несуществующая запись");
                return false;

            }
            else return true;
            
        }
        static void CreateNewNote(List<Note> notes)
        {
            Note note = new Note();          
            Console.WriteLine("Введите фамилию");
            note.Surname = Console.ReadLine();
            Console.WriteLine("Введите имя");
            note.Name = Console.ReadLine();
            Console.WriteLine("Введите отчество");
            Console.WriteLine("Поле не является обязательным, для пропуска нажмите Enter");
            note.MiddleName = Console.ReadLine();
            Console.WriteLine("Введите номер телефона");
            note.PhoneNumber = Console.ReadLine();
            Console.WriteLine("Введите страну");
            note.Country = Console.ReadLine();
            Console.WriteLine("Введите дату рождения");
            Console.WriteLine("Поле не является обязательным, значением по умолчанию установлена текущая дата, для пропуска нажмите Enter");
            note.BirthDay = Console.ReadLine();
            Console.WriteLine("Введите организацию");
            Console.WriteLine("Поле не является обязательным, для пропуска нажмите Enter");
            note.Organization = Console.ReadLine();
            Console.WriteLine("Введите должность");
            Console.WriteLine("Поле не является обязательным, для пропуска нажмите Enter");
            note.Position = Console.ReadLine();
            Console.WriteLine("Поле для прочих заметок");
            Console.WriteLine("Поле не является обязательным, для пропуска нажмите Enter");
            note.OtherNotes = Console.ReadLine();
            notes.Add(note);
            Console.WriteLine("Запись успешно добавлена!");
            EndPart();
        }
        static void EditNote(List<Note> notes, int id)
        {

            if (IfExist(notes, id, out Note note))
            {

                Console.WriteLine("Какое поле вы хотите изменить?\n 1- Фамилия\n 2-Имя \n 3-Отчество\n 4-Номер телефона\n 5-Страна \n 6-Дата рождения\n 7-Организация\n 8-Должность\n 9- Прочие заметки");
                if (Int32.TryParse(Console.ReadLine(), out int answer))
                {

                    switch (answer)
                    {
                        case 1:
                            Console.WriteLine("Введите новое значение");
                            note.Surname = Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("Введите новое значение");
                            note.Name = Console.ReadLine();
                            break;
                        case 3:
                            Console.WriteLine("Введите новое значение");
                            note.MiddleName = Console.ReadLine();
                            break;
                        case 4:
                            Console.WriteLine("Введите новое значение");
                            note.PhoneNumber = Console.ReadLine();
                            break;
                        case 5:
                            Console.WriteLine("Введите новое значение");
                            note.Country = Console.ReadLine();
                            break;
                        case 6:
                            Console.WriteLine("Введите новое значение");
                            note.BirthDay = Console.ReadLine();
                            break;
                        case 7:
                            Console.WriteLine("Введите новое значение");
                            note.Organization = Console.ReadLine();
                            break;
                        case 8:
                            Console.WriteLine("Введите новое значение");
                            note.Position = Console.ReadLine();
                            break;
                        case 9:
                            Console.WriteLine("Введите новое значение");
                            note.OtherNotes = Console.ReadLine();
                            break;



                        default:
                            Console.WriteLine("Несуществующее поле");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Несуществующая операция!!!");
                    EndPart();
                }
            }
            
            EndPart();
        }
        static void DeleteNote(List<Note> notes, int id)
        {
            if (IfExist(notes, id,out Note note))
            {
                notes.Remove(note);
                Console.WriteLine("Запись успешно удалена!");
                EndPart();
            }
            else
            {
                EndPart();
            }
            
        }
        static void ShowNote(List<Note> notes, int id)
        {
            if(IfExist(notes, id,out Note note))
            {
                Console.WriteLine("id:"+note.id);
                Console.WriteLine("Фамилия:"+note.Surname);
                Console.WriteLine("Имя:"+note.Name);
                Console.WriteLine("Отчество:"+note.MiddleName);
                Console.WriteLine("Номер телефона:"+note.PhoneNumber);
                Console.WriteLine("Страна:"+note.Country);
                Console.WriteLine("День рождения:"+note.BirthDay);
                Console.WriteLine("Организация:"+note.Organization);
                Console.WriteLine("Должность:"+note.Position);
                Console.WriteLine("Прочие заметки:"+note.OtherNotes);
            }
            EndPart();
            
        }
        static void ShowAll(List<Note> notes)
        {
            if (notes.Count != 0)
            {
                Console.WriteLine("{0,3}\t {1,15}\t {2,15}\t {3,15}","id","Фамилия","Имя","Номер телефона");
                foreach (Note note in notes)
                {

                    Console.WriteLine("{0,3}\t {1,15}\t {2,15}\t {3,15}", note.id, note.Surname, note.Name, note.PhoneNumber);
                }
                EndPart();
            }
            else {
                Console.WriteLine("Записей пока нет");
                EndPart();
                 }
        }
        static void Main(string[] args)
        {
            List<Note> notes = new List<Note>();


            
            while (true)
            {
                Console.WriteLine("Добро пожаловать в записную книжку!");
                Console.WriteLine("================================================");
                Console.WriteLine("NoteBook");
                Console.WriteLine("================================================");
                Console.WriteLine("Что хотите сделать?");
                Console.WriteLine("1 - Создать новую запись\n2 - Изменить существующую запись\n3 - Удалить запись\n4 - Показать выбранную запись\n5 - Показать все записи\n6 - Выйти");
                string output = Console.ReadLine();
                if (Int32.TryParse(output, out int answer))
                {
                    if (answer == 1)
                    {
                        Console.Clear();
                        CreateNewNote(notes);
                    }
                    else if (answer == 2)
                    {
                        
                        if (notes.Count != 0)
                        {
                            Console.Clear();
                            ShowAll(notes);
                            Console.WriteLine("Какую запись хотите изменить?");
                            Console.WriteLine("Введите id");
                            if (Int32.TryParse(Console.ReadLine(), out int id))
                            {
                                EditNote(notes, id);
                            }
                            else Console.WriteLine("Неверный формат!");

                        }
                        else Console.WriteLine("Записей нет");
                        EndPart();
                    }
                    else if (answer == 3)
                    {
                        if (notes.Count != 0)
                        {
                            Console.Clear();
                            ShowAll(notes);
                            Console.WriteLine("Какую запись хотите удалить?");
                            Console.WriteLine("Введите id");
                            if (Int32.TryParse(Console.ReadLine(), out int id))
                            {
                                DeleteNote(notes, id);
                            }
                            else Console.WriteLine("Неверный формат!");
                        }
                        else Console.WriteLine("Записей нет");
                        EndPart();
                    }
                    else if (answer == 4)
                    {
                        if (notes.Count != 0)
                        {
                            Console.Clear();
                            ShowAll(notes);
                            Console.WriteLine();
                            Console.WriteLine("Какую запись показать?");
                            Console.WriteLine("Введите id");
                            if (Int32.TryParse(Console.ReadLine(), out int id))
                            {
                                
                                ShowNote(notes, id);
                            }
                            else Console.WriteLine("Неверный формат!");
                        }
                        else Console.WriteLine("Записей нет");
                        EndPart();
                    }
                    else if (answer == 5)
                    {
                        Console.Clear();
                        ShowAll(notes);
                    }
                    else if (answer == 6)
                    {
                        Console.WriteLine("Удачного дня!Прощайте!!!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Несуществующая операция!!!");
                        EndPart();
                    }
                }else
                {
                    Console.WriteLine("Несуществующая операция!!!");
                    EndPart();
                }






            }

        }
    }
}
