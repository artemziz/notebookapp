﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoteBookApp
{
    class Note
    {
        private static int count = 0;
        public int id;
        private string surname;
        public string Surname {
            get { return this.surname; }
            set
            {
                while (value.Length == 0)
                {
                    Console.WriteLine("Пустое значение!!!Введите фамилию");
                    value = Console.ReadLine();
                }
                this.surname = value;

            }
        }
        private string name;
        public string Name {
            get { return this.name; }
            set
            {
                while (value.Length == 0)
                {
                    Console.WriteLine("Пустое значение!!!Введите имя");
                    value = Console.ReadLine();
                }
                this.name = value;

            }
        }
        public string MiddleName { get; set; }
        private string phoneNumber;
        public string PhoneNumber {

            get { return this.phoneNumber; }
            set
            {
                while (!long.TryParse(value, out long phoneNumber))
                {
                    Console.WriteLine("Некорректный формат ввода!");
                    Console.WriteLine("Введите корректный номер телефона");
                    value = Console.ReadLine();
                }
                this.phoneNumber = value;
            }
        }
        
        private string country;
        public string Country {
            get { return this.country; }
            set
            {
                while (value.Length == 0)
                {
                    Console.WriteLine("Пустое значение!!!Введите страну");
                    value = Console.ReadLine();
                }
                this.country = value;

            }
        }
        private string birthDay;
        public string BirthDay
        {
            get { return this.birthDay; }
            set {
                if (value.Length != 0)
                {
                    if (!DateTime.TryParse(value, out DateTime birthDay))
                    {
                        Console.WriteLine("Некорректный формат ввода!");
                    }
                    else
                    {
                        this.birthDay = DateTime.Parse(value).ToShortDateString();
                       
                    }
                }
            }
        }
        
        public string Organization { get; set; }
        public string Position { get; set; }
        public string OtherNotes { get; set; }

        public Note()
        {
            this.id = count++;
            this.MiddleName = String.Empty;
            this.birthDay = DateTime.Now.ToShortDateString();
            this.Organization = String.Empty;
            this.Position = String.Empty;
            this.OtherNotes = String.Empty;

        }
    }
}
